package by.bsuir.controllers;

import by.bsuir.model.*;
import by.bsuir.service.ServicesService;
import by.bsuir.service.TariffService;
import by.bsuir.service.UserService;
import by.bsuir.service.mail.EmailSender;
import by.bsuir.service.mail.EmailTemplater;
import by.bsuir.service.mail.MessageBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Locale;

@Controller
public class OperatorController extends BaseController {

    @Autowired
    ServicesService servicesService;
    @Autowired
    UserService userService;
    @Autowired
    TariffService tariffService;

    @Autowired
    EmailTemplater emailTemplater;

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private MessageBuilder messageBuilder;


    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    @RequestMapping(value = "/operator/users_list", method = RequestMethod.GET)
    public String usersList(ModelMap modelMap, Locale locale) {

        UsersEntity user = userService.getUserByLogin(authUser());

        List<User> users = userService.getAllOperatorUsers(user.getId());
        System.out.println(users.size());

        modelMap.addAttribute("users", users);
        modelMap.addAttribute("templatesCount", emailTemplater.getTemplates().size());

        String title = messageSource.getMessage("Main", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/users_list.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/send_mail", method = RequestMethod.POST)
    @ResponseBody
    public Operator sendMail(ModelMap modelMap, @ModelAttribute StatObject stat) {

        if (stat.getInt_list() != null) {
            for (Integer user_id : stat.getInt_list()) {

                try {
                    User u = userService.getUserrrById(user_id);

                    String letter = emailTemplater.getEmail(stat.getField_1(), stat.getString_2(), u.getFio(),stat.getString_1());

                    System.out.println(letter);

                    new EmailSender(mailSender).set(
                            messageBuilder.init(u, mailSender).
                                    subject(stat.getString_1()).
                                    body(letter, true).
                                    construct()
                    ).start();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }


        System.out.println(stat);

        return null;
    }


    //////////////////// ok

    @RequestMapping(value = "/operator/edit_info", method = RequestMethod.GET)
    public String editOperatorInfo(ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("edit_info", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        Operator operatorInfo = userService.getOperatorByLogin(this.authUser());

        modelMap.addAttribute("operatorInfo", operatorInfo);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/edit_operator.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/save_operator", method = RequestMethod.POST)
    @ResponseBody
    public Operator editOperatorPOST(ModelMap modelMap, @ModelAttribute Operator operator) {
        if (operator.isValid()) {

            Operator oldOperator = userService.getOperatorById(operator.getId());

            operator.setServices(oldOperator.getServices());
            operator.setTarifs(oldOperator.getTarifs());

            Operator newUser = (Operator) userService.saveUser(operator);
            return newUser;
        }
        return null;
    }


    @RequestMapping(value = "/operator", method = RequestMethod.GET)
    public String index(ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("Main", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/index.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/services_list", method = RequestMethod.GET)
    public String servicesList(ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("services_list", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        Operator operator = userService.getOperatorByLogin(this.authUser());

        List<ServiceEntity> servicesList = servicesService.getOperatorServices(operator);
        modelMap.addAttribute("servicesList", servicesList);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/services_list.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/edit_service/{id}", method = RequestMethod.GET)
    public String editService(@PathVariable Integer id, ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("edit_service", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        ServiceEntity serviceEntity = servicesService.getById(id);

        modelMap.addAttribute("serviceEntity", serviceEntity);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_service.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/add_service", method = RequestMethod.GET)
    public String addService(ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("add_service", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        modelMap.addAttribute("userName", this.authUser());

        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_service.jsp");
        return "Template";
    }


    // список тарифов с возм удаления
    @RequestMapping(value = "/operator/tariffs_list", method = RequestMethod.GET)
    public String tariffsList(ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("tariffs_list", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        Operator operator = userService.getOperatorByLogin(this.authUser());

        List<TariffEntity> tariffs = tariffService.getOperatorTariffs(operator);

        System.out.println(tariffs.size());

        modelMap.addAttribute("tariffsList", tariffs);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/tariffs_list.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/edit_tariff/{id}", method = RequestMethod.GET)
    public String editTariff(@PathVariable Integer id, ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("edit_tariff", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        TariffEntity tariffEntity = tariffService.getById(id);
        modelMap.addAttribute("tariffEntity", tariffEntity);

        Operator operator = userService.getOperatorByLogin(this.authUser());

        List<ServiceEntity> servicesList = servicesService.getOperatorServices(operator);

        modelMap.addAttribute("servicesList", servicesList);


        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_tariff.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/add_tariff", method = RequestMethod.GET)
    public String addTariff(ModelMap modelMap, Locale locale) {

        String title = messageSource.getMessage("add_tariff", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_tariff.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/save_service", method = RequestMethod.POST)
    @ResponseBody
    public ServiceEntity addOperatorPOST(ModelMap modelMap, @ModelAttribute ServiceEntity serviceEntity) {
        if (serviceEntity.isValid()) {

            // temp
            Operator operator = userService.getOperatorByLogin(this.authUser());
            serviceEntity.setOwner(operator);
            return servicesService.save(serviceEntity);
        }
        return null;
    }


    @RequestMapping(value = "/operator/delete_tariff/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String deleteTariff(@PathVariable Integer id, ModelMap modelMap) {
        // temp
        Operator operator = userService.getOperatorByLogin(this.authUser());

        TariffEntity tariffEntity = tariffService.getById(id);
        tariffEntity.setServices(null);
        tariffService.save(tariffEntity);

        if (id != null) {

            try {

                List<User> users = userService.getAllUsersByTariff(id);

                tariffService.delete(id);

                if (users != null) {

                    for (User u : users) {
                        try {
                            new EmailSender(mailSender).set(
                                    messageBuilder.init(u, mailSender).
                                            subject("DELETED TARIFF").
                                            body("<html><head><meta charset=\"UTF-8\"></head><body>" +
                                                    "<h3> Hi, " + u.getFio() + "</h3>" +
                                                    "<h3> Theme: DELETE TARIFF FOR " + operator.getTitle() + " operator </h3>" +
                                                    "</body></html>", true).
                                            construct()
                            ).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return "{\"result\":\"true\"}";
        }
        return "{\"result\":\"true\"}";
    }

    @RequestMapping(value = "/operator/delete_services", method = RequestMethod.POST)
    @ResponseBody
    public String deleteServicesByCheckbox(ModelMap modelMap, @ModelAttribute StatObject stat) {

        Operator operator = userService.getOperatorByLogin(this.authUser());
        ServiceEntity serviceEntity;

        if (stat.getInt_list() != null) {
            for (Integer serviceId : stat.getInt_list()) {

                serviceEntity = servicesService.getById(serviceId);

                if (serviceEntity != null && serviceEntity.getOwner().getId().equals(operator.getId())) {
                    servicesService.delete(serviceEntity);
                }

            }
        }


        System.out.println(stat);

        return null;
    }


    @RequestMapping(value = "/operator/delete_service/{id}", method = RequestMethod.POST)
    public Boolean deleteService(@PathVariable Integer id, ModelMap modelMap) {
        // temp
        Operator operator = userService.getOperatorByLogin(this.authUser());

        ServiceEntity serviceEntity = servicesService.getById(id);

        if (serviceEntity != null && serviceEntity.getOwner().getId().equals(operator.getId())) {
            return servicesService.delete(serviceEntity);
        }

        return false;
    }


    @RequestMapping(value = "/operator/save_tariff", method = RequestMethod.POST)
    @ResponseBody
    public TariffEntity addTariffPOST(ModelMap modelMap, @ModelAttribute TariffEntity tariffEntity) {
        if (tariffEntity.isValid()) {

            if (tariffEntity.getId() != null) {
                TariffEntity oldTariff = tariffService.getById(tariffEntity.getId());
                tariffEntity.setServices(oldTariff.getServices());
            }


            // temp
            Operator operator = userService.getOperatorByLogin(this.authUser());

            tariffEntity.setOwner(operator);


            return tariffService.save(tariffEntity);
        }
        return null;
    }


    @RequestMapping(value = "/operator/save_or_delete_service_in_tariff", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addOrDeleteServiceInTariffPOST(ModelMap modelMap, @ModelAttribute StatObject statObject) {

        TariffEntity tariffEntity = tariffService.getById(statObject.getField_2());

        if (statObject.getString_1().equals("false")) {
            for (ServiceEntity serviceEntity : tariffEntity.getServices()) {
                if (serviceEntity.getId().equals(statObject.getField_1())) {
                    tariffEntity.getServices().remove(serviceEntity);
                    break;
                }
            }
        } else {
            ServiceEntity serviceEntity = servicesService.getById(statObject.getField_1());
            tariffEntity.getServices().add(serviceEntity);
        }

        tariffService.save(tariffEntity);

        return true;
    }


}
