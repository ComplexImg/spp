package by.bsuir.controllers;

import by.bsuir.model.*;
import by.bsuir.service.PaymentService;
import by.bsuir.service.ServicesService;
import by.bsuir.service.TariffService;
import by.bsuir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Locale;

@Controller
public class UserController extends BaseController {

    @Autowired
    ServicesService servicesService;
    @Autowired
    UserService userService;
    @Autowired
    TariffService tariffService;
    @Autowired
    PaymentService paymentService;

    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/user/add_operator/{id}", method = RequestMethod.GET)
    public String addOperator(@PathVariable Integer id, ModelMap modelMap, Locale locale) {

        String title =  messageSource.getMessage("add_operator", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        User user = userService.gettUserrrByLogin(this.authUser());

        Operator operator = userService.getFullOperatorById(id);

        modelMap.addAttribute("operator", operator);
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "user/add_operator.jsp");
        return "Template";
    }


    @RequestMapping(value = "/user/add_operator", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addOperatorPOST(ModelMap modelMap, @ModelAttribute StatObject statObject) {

        Operator operator = userService.getOperatorById(statObject.getField_2());
        User user = userService.getUserrrById(statObject.getField_1());
        Integer tariff_id = statObject.getField_3();

        user.getOperators().add(operator);

        userService.saveUser(user);

        userService.updateUserOperatorTarif(statObject.getField_1(), statObject.getField_2(), tariff_id);


        return true;
//        return userService.updateUserOperatorTarif(user_id, operator_id, tariff_id);
    }


    /////////// okkk

    @RequestMapping(value = "/user/payments_history", method = RequestMethod.GET)
    public String paymentsHistory(ModelMap modelMap, Locale locale) {

        String title =  messageSource.getMessage("payment_history", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        User user = userService.gettUserrrByLogin(this.authUser());

        List<PaymentEntity> payments = paymentService.getUserPayments(user.getId());


        modelMap.addAttribute("payments", payments);
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "user/payments_history.jsp");
        return "Template";
    }


    @RequestMapping(value = "/user/add_payment", method = RequestMethod.GET)
    public String addPayment(ModelMap modelMap, Locale locale) {

        String title =  messageSource.getMessage("add_payment", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        User user = userService.gettUserrrByLogin(this.authUser());

        modelMap.addAttribute("user", user);
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "user/add_payment.jsp");
        return "Template";
    }


    @RequestMapping(value = "/user/add_payment", method = RequestMethod.POST)
    @ResponseBody
    public PaymentEntity addPaymentPOST(ModelMap modelMap, @ModelAttribute StatObject statObject) {

        PaymentEntity paymentEntity = new PaymentEntity();
        paymentEntity.setAmount(statObject.getField_3());
        paymentEntity.setOperator_id(statObject.getField_2());
        paymentEntity.setUser_id(statObject.getField_1());

        return paymentService.addPayment(paymentEntity);
    }


    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String index(ModelMap modelMap, Locale locale) {

        String title =  messageSource.getMessage("Main", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "user/index.jsp");
        return "Template";
    }


    @RequestMapping(value = "/user/operators_list", method = RequestMethod.GET)
    public String operatorsList(ModelMap modelMap, Locale locale) {

        String title =  messageSource.getMessage("operators_list", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        List<Operator> operatorsList = userService.getOperatorsList();

        User user = userService.gettUserrrByLogin(this.authUser());


        modelMap.addAttribute("operatorsList", operatorsList);
        modelMap.addAttribute("user", user);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "user/operators_list.jsp");
        return "Template";
    }


    @RequestMapping(value = "/user/edit_operator/{id}", method = RequestMethod.GET)
    public String editOperator(@PathVariable Integer id, ModelMap modelMap, Locale locale) {

        String title =  messageSource.getMessage("edit_operator", new Object[]{}, locale);
        modelMap.addAttribute("title", title);


        User user = userService.gettUserrrByLogin(this.authUser());

        Operator operator = userService.getFullOperatorById(id);


//        userService.updateUserOperatorTarif(user.getId(), operator.getId(), 5);
        Integer operatorUserTariffId = userService.userOperatorTarif(user.getId(), operator.getId());

        modelMap.addAttribute("operator", operator);
        modelMap.addAttribute("operatorUserTariffId", operatorUserTariffId);
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "user/edit_operator.jsp");
        return "Template";
    }


    @RequestMapping(value = "/user/save_or_delete_service_in_user", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addOrDeleteServiceInUserPOST(ModelMap modelMap, @ModelAttribute StatObject statObject) {

        User user = userService.getUserrrById(statObject.getField_2());


        if (statObject.getString_1().equals("false")) {
            for (ServiceEntity serviceEntity : user.getServices()) {
                if (serviceEntity.getId().equals(statObject.getField_1())) {
                    user.getServices().remove(serviceEntity);
                    break;
                }
            }
        } else {
            ServiceEntity serviceEntity = servicesService.getById(statObject.getField_1());
            user.getServices().add(serviceEntity);
        }

        userService.saveUser(user);


        return true;
    }


    @RequestMapping(value = "/user/update_user_operator_tariff", method = RequestMethod.POST)
    @ResponseBody
    public Boolean updateUserOperatorTariffPOST(ModelMap modelMap, @ModelAttribute StatObject statObject) {

        Integer tariff_id = statObject.getField_1();
        Integer user_id = statObject.getField_2();
        Integer operator_id = statObject.getField_3();

        return userService.updateUserOperatorTarif(user_id, operator_id, tariff_id);
    }

}


