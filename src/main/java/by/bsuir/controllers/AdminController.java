package by.bsuir.controllers;

import by.bsuir.model.Operator;
import by.bsuir.model.UsersEntity;
import by.bsuir.service.TariffService;
import by.bsuir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;


@Controller
public class AdminController extends BaseController {

    @Autowired
    UserService userService;

    @Autowired
    TariffService tariffService;

    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String index(ModelMap modelMap, Locale locale)
    {
        String title =  messageSource.getMessage("main_adminpanel", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "admin/index.jsp");
        return "Template";
    }


    @RequestMapping(value = "/admin/operators_list", method = RequestMethod.GET)
    public String operatorsList(ModelMap modelMap, Locale locale)
    {
        String title =  messageSource.getMessage("operators_list", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        List<Operator> operatorsList = userService.getOperatorsList();

        modelMap.addAttribute("operatorsList", operatorsList);
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "admin/operators_list.jsp");
        return "Template";
    }


    // стр с формой добавления
    @RequestMapping(value = "/admin/new_operator", method = RequestMethod.GET)
    public String addOperator(ModelMap modelMap, Locale locale)
    {
        String title =  messageSource.getMessage("new_operator", new Object[]{}, locale);
        modelMap.addAttribute("title", title);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "admin/new_operator.jsp");
        return "Template";
    }


    @RequestMapping(value = "/admin/save_operator", method = RequestMethod.POST)
    @ResponseBody
    public Operator addOperatorPOST(ModelMap modelMap, @ModelAttribute Operator operator) {
        if (operator.isValid()) {
            UsersEntity findUser = userService.getUserByLogin(operator.getUsername());

            if(findUser == null){
                Operator newUser = (Operator) userService.saveUser(operator);
                return newUser;
            }
        }
        return null;
    }


    @RequestMapping(value = "/admin/delete_operator/{id}", method = RequestMethod.POST)
    public Boolean deleteOperator(@PathVariable Integer id, ModelMap modelMap) {
        // temp
        UsersEntity usersEntity = userService.getUserById(id);

        if(usersEntity != null){
            userService.delete(usersEntity);
            return true;
        }

        return false;
    }

}
