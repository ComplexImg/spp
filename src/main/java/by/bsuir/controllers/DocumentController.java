package by.bsuir.controllers;

import by.bsuir.model.*;
import by.bsuir.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Controller
public class DocumentController {

    @Autowired
    ReportGenerationService reportGenerationService;

    @Autowired
    UserService userService;

    @Autowired
    PaymentService paymentService;

    @Autowired
    TariffService tariffService;

    @Autowired
    ServicesService servicesService;

    private final String USER_CONTRACT_FILE_NAME = "User contract";
    private final String PAYMENT_QUOTE_FILE_NAME = "Payment quote";
    private final String PAYMENTS_FILE_NAME = "Payments report";
    private final String USER_SERVICES_FILE_NAME = "User services report";
    private final String OPERATOR_SERVICES_FILE_NAME = "Operator services";

    @RequestMapping(value = "/reports/user_contract/{user_login}/{tariff_id}/{format}", method = RequestMethod.GET)
    public void generateUserContractReport(@PathVariable String user_login,
                                           @PathVariable String tariff_id,
                                           @PathVariable("format") String format,
                                           HttpServletResponse response) {

        try {
            User user = userService.getUserrrById(Integer.parseInt(user_login));
            TariffEntity tariff = tariffService.getById(Integer.parseInt(tariff_id));
            int formatCode = getFormatCode(format);
            response.setContentType(getResponseContentType(formatCode));
            response.setHeader("content-disposition", "attachment;filename=" + USER_CONTRACT_FILE_NAME + GetFileExtension(formatCode));
            reportGenerationService.generateUserContract(response.getOutputStream(), user, tariff, new Date(), formatCode);
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/reports/payment_quote/{user_login}/{paym_id}/{format}", method = RequestMethod.GET)
    public void generatePaymentQuoteReport(@PathVariable String user_login,
                                           @PathVariable String paym_id,
                                           @PathVariable("format") String format,
                                           HttpServletResponse response) {
        try {
            User user = userService.getUserrrById(Integer.parseInt(user_login));
            List<PaymentEntity> userPayments = paymentService.getUserPayments(user.getId());
            PaymentEntity payment = null;
            for (PaymentEntity pmt: userPayments) {
                if (pmt.getId() == Integer.parseInt(paym_id)) {
                    payment = pmt;
                    break;
                }
            }
            if (payment != null) {
                Operator operator = userService.getOperatorById(payment.getOperator_id());
                int formatCode = getFormatCode(format);
                response.setContentType(getResponseContentType(formatCode));
                response.setHeader("content-disposition", "attachment;filename=" + PAYMENT_QUOTE_FILE_NAME + GetFileExtension(formatCode));
                reportGenerationService.generatePaymentQuote(response.getOutputStream(), user, payment, operator, new Date(), formatCode);
                response.flushBuffer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/reports/user_payments/{user_id}/{format}", method = RequestMethod.GET)
    public void generateUserPaymentsReport(@PathVariable("user_id") String user_id,
                                           @PathVariable("format") String format,
                                           HttpServletResponse response) {
        try {
            User user = userService.getUserrrById(Integer.parseInt(user_id));
            List<PaymentEntity> userPayments = paymentService.getUserPayments(user.getId());
            int formatCode = getFormatCode(format);
            response.setContentType(getResponseContentType(formatCode));
            response.setHeader("content-disposition", "attachment;filename=" + PAYMENTS_FILE_NAME + GetFileExtension(formatCode));
            reportGenerationService.generateUserPaymentsReport(response.getOutputStream(), userPayments, formatCode, new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/reports/user_services/{user_id}/{format}", method = RequestMethod.GET)
    public void generateUserServicesReport(@PathVariable("user_id") String user_id,
                                           @PathVariable("format") String format,
                                           HttpServletResponse response) {
        try {
            User user = userService.getUserrrById(Integer.parseInt(user_id));
            int formatCode = getFormatCode(format);
            response.setContentType(getResponseContentType(formatCode));
            response.setHeader("content-disposition", "attachment;filename=" + USER_SERVICES_FILE_NAME + GetFileExtension(formatCode));
            reportGenerationService.generateUserServicesReport(response.getOutputStream(), user, formatCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/reports/all_services/{operator_id}/{format}", method = RequestMethod.GET)
    public void generateAllOperatorServicesList(@PathVariable("operator_id") String operator_id,
                                                @PathVariable("format") String format,
                                                HttpServletResponse response) {
        try {
            Operator operator = userService.getFullOperatorById(Integer.parseInt(operator_id));
            int formatCode = getFormatCode(format);
            response.setContentType(getResponseContentType(formatCode));
            response.setHeader("content-disposition", "attachment;filename=" + OPERATOR_SERVICES_FILE_NAME + GetFileExtension(formatCode));
            reportGenerationService.generateListOfAllOperatorServices(response.getOutputStream(), operator, formatCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getFormatCode(String format) {
        format = format.trim().toUpperCase();
        switch (format) {
            case "CSV": {
                return ReportGenerationServiceImpl.CSV;
            }
            case "DOCX": {
                return ReportGenerationServiceImpl.DOCX;
            }
            case "PNG": {
                return ReportGenerationServiceImpl.PNG;
            }
            case "PDF": {
                return ReportGenerationServiceImpl.PDF;
            }
            case "XLS": {
                return ReportGenerationServiceImpl.XLS;
            }
            default:{
                return ReportGenerationServiceImpl.PDF;
            }
        }
    }

    private String getResponseContentType(int formatCode) {
        switch (formatCode) {
            case ReportGenerationServiceImpl.PDF: {
                return "application/pdf";
            }
            case ReportGenerationServiceImpl.CSV: {
                return "text/csv";
            }
            case ReportGenerationServiceImpl.DOCX: {
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            }
            case ReportGenerationServiceImpl.PNG: {
                return "image/png";
            }
            case  ReportGenerationServiceImpl.XLS: {
                return "application/vnd.ms-excel";
            }
            default:{
                return "";
            }
        }
    }

    private String GetFileExtension(int formatCode) {
        switch (formatCode) {
            case ReportGenerationServiceImpl.PDF: {
                return ".pdf";
            }
            case ReportGenerationServiceImpl.CSV: {
                return ".csv";
            }
            case ReportGenerationServiceImpl.DOCX: {
                return ".docx";
            }
            case ReportGenerationServiceImpl.PNG: {
                return ".png";
            }
            case ReportGenerationServiceImpl.XLS: {
                return ".xls";
            }
            default:{
                return "";
            }
        }
    }
}
