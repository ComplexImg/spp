package by.bsuir.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tariff", schema = "mydb", catalog = "")
public class TariffEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Integer id = null;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "operator_id", referencedColumnName = "id")
    private Operator owner;

    @Basic
    @Column(name = "title")
    private String title;

    @Basic
    @Column(name = "cost_per_month")
    private Integer costPerMonth;

    @Basic
    @Column(name = "description")
    private String description;

    @Transient
    public boolean isServiceInTariffServices(ServiceEntity serviceEntity)
    {
        for(ServiceEntity service : services){
            if(service.getId().equals(serviceEntity.getId())){
                return true;
            }
        }
        return false;
    }

    @Transient
    public boolean isValid() {

        return (
                title.length() > 0 &&
                        description.length() > 0 &&
                        costPerMonth != null
        );
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name="tariff_services",
            joinColumns=@JoinColumn(name="tariff_id"),
            inverseJoinColumns=@JoinColumn(name="service_id"))
    private Set<ServiceEntity> services = new HashSet<ServiceEntity>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCostPerMonth() {
        return costPerMonth;
    }

    public void setCostPerMonth(Integer costPerMonth) {
        this.costPerMonth = costPerMonth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Operator getOwner() {
        return owner;
    }

    public void setOwner(Operator owner) {
        this.owner = owner;
    }

    public Set<ServiceEntity> getServices() {
        return services;
    }

    public void setServices(Set<ServiceEntity> services) {
        this.services = services;
    }
}
