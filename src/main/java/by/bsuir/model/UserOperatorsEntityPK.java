package by.bsuir.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Tom on 14.03.2018.
 */
public class UserOperatorsEntityPK implements Serializable {
    private int userId;
    private int operatorId;

    @Column(name = "user_id")
    @Id
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "operator_id")
    @Id
    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserOperatorsEntityPK that = (UserOperatorsEntityPK) o;

        if (userId != that.userId) return false;
        if (operatorId != that.operatorId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + operatorId;
        return result;
    }
}
