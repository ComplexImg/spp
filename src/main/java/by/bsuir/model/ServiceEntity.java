package by.bsuir.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "service", schema = "mydb", catalog = "")
public class ServiceEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "title")
    private String title;

    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "USSD_code")
    private String ussdCode;

    @Basic
    @Column(name = "cost_per_month")
    private Integer costPerMonth;


    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "operator_id", referencedColumnName = "id")
    private Operator owner;

    @Transient
    public boolean isValid() {

        return (
                title.length() > 0 &&
                        description.length() > 0 &&
                        ussdCode.length() > 0 &&
                        costPerMonth != null
        );
    }

    //
//    @ManyToMany
//    private Set<TariffEntity> tariffs = new HashSet<TariffEntity>();

//    public Set<TariffEntity> getTariffs() {
//        return tariffs;
//    }
//
//    public void setTariffs(Set<TariffEntity> tariffs) {
//        this.tariffs = tariffs;
//    }

    public Operator getOwner() {
        return owner;
    }

    public void setOwner(Operator owner) {
        this.owner = owner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUssdCode() {
        return ussdCode;
    }

    public void setUssdCode(String ussdCode) {
        this.ussdCode = ussdCode;
    }

    public Integer getCostPerMonth() {
        return costPerMonth;
    }

    public void setCostPerMonth(Integer costPerMonth) {
        this.costPerMonth = costPerMonth;
    }

}
