package by.bsuir.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("operator")
@SecondaryTable(schema = "mydb", name = "operator_info", pkJoinColumns = {@PrimaryKeyJoinColumn})
public class Operator extends UsersEntity {

    public static String DEF_ROLE = "ROLE_OPERATOR";

    @PrePersist
    public void prePersist() {
        if (roles.isEmpty())
            roles.add(DEF_ROLE);
        setDate(Timestamp.valueOf(LocalDateTime.now()));
    }

    @OneToMany(mappedBy = "owner", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<TariffEntity> tarifs = new ArrayList<TariffEntity>();

    @OneToMany(mappedBy = "owner", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ServiceEntity> services = new ArrayList<ServiceEntity>();

    @Column(name = "title", table = "operator_info")
    private String title;

    @Column(name = "description", table = "operator_info")
    private String description;

    public List<TariffEntity> getTarifs() {
        return tarifs;
    }

    public void setTarifs(List<TariffEntity> tarifs) {
        this.tarifs = tarifs;
    }

    public List<ServiceEntity> getServices() {
        return services;
    }

    public void setServices(List<ServiceEntity> services) {
        this.services = services;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
