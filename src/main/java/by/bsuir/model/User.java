package by.bsuir.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@DiscriminatorValue("user")
@SecondaryTable(schema = "mydb",name="user_info", pkJoinColumns={@PrimaryKeyJoinColumn})
public class User extends UsersEntity {

    public static String DEF_ROLE = "ROLE_USER";

    @PrePersist
    public void prePersist(){
        if(roles.isEmpty())
            roles.add(DEF_ROLE);
        setDate(Timestamp.valueOf(LocalDateTime.now()));
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="user_services",
            joinColumns=@JoinColumn(name="s_user_id"),
            inverseJoinColumns=@JoinColumn(name="s_service_id"))
    private Set<ServiceEntity> services = new HashSet<ServiceEntity>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="user_operators",
            joinColumns=@JoinColumn(name="user_id"),
            inverseJoinColumns=@JoinColumn(name="operator_id"))
    private Set<Operator> operators = new HashSet<Operator>();

    public Set<ServiceEntity> getServices() {
        return services;
    }

    public void setServices(Set<ServiceEntity> services) {
        this.services = services;
    }

    public Set<Operator> getOperators() {
        return operators;
    }

    public void setOperators(Set<Operator> operators) {
        this.operators = operators;
    }

    @Column(name = "fio", table="user_info")
    private String fio;

    @Column(name = "town", table="user_info")
    private String town;

    @Column(name = "other", table="user_info")
    private String other;

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    @Transient
    public boolean isOperatorInUser(Operator operator)
    {
        for(Operator op : operators){
            if(op.getId().equals(operator.getId())){
                return true;
            }
        }
        return false;
    }

    @Transient
    public boolean isServiceInUser(ServiceEntity serviceEntity)
    {
        for(ServiceEntity service : services){
            if(service.getId().equals(serviceEntity.getId())){
                return true;
            }
        }
        return false;
    }

}
