package by.bsuir.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Tom on 14.03.2018.
 */
public class TariffServicesEntityPK implements Serializable {
    private int tariffId;
    private int serviceId;

    @Column(name = "tariff_id")
    @Id
    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    @Column(name = "service_id")
    @Id
    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TariffServicesEntityPK that = (TariffServicesEntityPK) o;

        if (tariffId != that.tariffId) return false;
        if (serviceId != that.serviceId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tariffId;
        result = 31 * result + serviceId;
        return result;
    }
}
