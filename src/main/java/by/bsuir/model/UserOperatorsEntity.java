package by.bsuir.model;

import javax.persistence.*;

/**
 * Created by Tom on 14.03.2018.
 */
@Entity
@Table(name = "user_operators", schema = "mydb", catalog = "")
@IdClass(UserOperatorsEntityPK.class)
public class UserOperatorsEntity {
    private int userId;
    private int operatorId;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "operator_id")
    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserOperatorsEntity that = (UserOperatorsEntity) o;

        if (userId != that.userId) return false;
        if (operatorId != that.operatorId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + operatorId;
        return result;
    }
}
