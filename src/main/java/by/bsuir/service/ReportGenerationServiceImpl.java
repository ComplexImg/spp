package by.bsuir.service;

import by.bsuir.model.*;
import com.aspose.words.*;
import com.opencsv.CSVWriter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.ArrayUtil;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.List;

@Service("reportGenerationService")
public class ReportGenerationServiceImpl implements ReportGenerationService {

    private final String USER_CONTRACT_FILE_TEMPLATE = "User Contract.docx";
    private final String PAYMENT_QUOTE_FILE_TEMPLATE = "Payment Quote.docx";

    public final static int CSV = 0;
    public final static int PDF = SaveFormat.PDF;
    public final static int DOCX = SaveFormat.DOCX;
    public final static int PNG = SaveFormat.PNG;
    public final static int XLS = -1;

    public ReportGenerationServiceImpl() throws Exception {
        try {
            License license = new License();
            ClassLoader classLoader = getClass().getClassLoader();
            license.setLicense(classLoader.getResourceAsStream("Aspose.Words.lic"));
        } catch (FileNotFoundException e) {
            throw new Exception("Aspose license file not found", e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Aspose license activating exception", e);
        }
    }

    @Override
    public void generateUserPaymentsReport(OutputStream stream, List<PaymentEntity> payments, int saveFormat, Date date) throws Exception {
        String[] headers = new String[]{"Operator Id", "Amount", "Date"};

        List<String[]> paymentsData = new ArrayList();
        for (PaymentEntity transaction: payments) {
            paymentsData.add(new String[]{
                    transaction.getOperator_id().toString(), transaction.getAmount().toString(), date.toString()
            });
        }

        createDocumentWithSingleTable(stream, headers, paymentsData, saveFormat);
    }

    @Override
    public void generateUserServicesReport(OutputStream stream, User user, int saveFormat) throws Exception {
        String[] headers = new String[]{"Title", "Description", "Ussd Code", "Cost Per Month"};

        List<String[]> servicesData = new ArrayList();
        for (ServiceEntity service: user.getServices()) {
            servicesData.add(new String[]{
                    service.getTitle(), service.getDescription(), service.getUssdCode(), service.getCostPerMonth().toString()
            });
        }

        createDocumentWithSingleTable(stream, headers, servicesData, saveFormat);
    }

    @Override
    public void generateListOfAllOperatorServices(OutputStream stream, Operator operator, int saveFormat) throws Exception {
        String[] headers = new String[]{"Title", "Description", "Ussd Code", "Cost Per Month"};

        List<String[]> servicesData = new ArrayList();
        for (ServiceEntity service: operator.getServices()) {
            servicesData.add(new String[]{
                    service.getTitle(), service.getDescription(), service.getUssdCode(), service.getCostPerMonth().toString()
            });
        }

        createDocumentWithSingleTable(stream, headers, servicesData, saveFormat);
    }

    @Override
    public void generateUserContract(OutputStream stream, User user, TariffEntity tariff, Date date, int saveFormat) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        Document docTemplate = new Document(classLoader.getResourceAsStream(USER_CONTRACT_FILE_TEMPLATE));

        int totalCost = 0;
        for (ServiceEntity service: tariff.getServices()) {
            totalCost += service.getCostPerMonth();
        }

        String finalTotalCost = Integer.toString(totalCost);
        Map<String, String> fieldValues = new HashMap<String, String>(){{
            put("OperatorTitle", tariff.getOwner().getTitle());
            put("TariffTitle", tariff.getTitle());
            put("TariffDescription", tariff.getDescription());
            put("UserName", user.getFio());
            put("UserEmail", user.getEmail());
            put("ContractDate", date.toString());
            put("TotalCost", finalTotalCost);
        }};

        List<String[]> tableFieldValues = new ArrayList<>();
        for (ServiceEntity service: tariff.getServices()) {
            tableFieldValues.add(new String[]{
                    service.getTitle(),
                    service.getDescription(),
                    service.getCostPerMonth().toString()
            });
        }

        String[] headers = new String[]{"Title", "Desription", "Price"};

        if (saveFormat == SaveFormat.PDF || saveFormat == SaveFormat.DOCX) {
            fillValuesInDocumentTemplate(docTemplate, fieldValues);

            DocumentBuilder builder = new DocumentBuilder(docTemplate);
            builder.moveToBookmark("ServicesTable");
            createTableInWordDocument(builder, headers, tableFieldValues);

            docTemplate.save(stream, saveFormat);
        } else if (saveFormat == XLS) {
            saveWordDocumentToExcel(stream, fieldValues);
        }
    }

    @Override
    public void generatePaymentQuote(OutputStream stream, User user, PaymentEntity payment, Operator operator, Date date, int saveFormat) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        Document docTemplate = new Document(classLoader.getResourceAsStream(PAYMENT_QUOTE_FILE_TEMPLATE));

        Map<String, String> fieldValues = new HashMap<String, String>(){{
            put("Operator", operator.getTitle());
            put("OperatorDescription", operator.getDescription());
            put("UserName", user.getFio());
            put("UserEmail", user.getEmail());
            put("PaymentAmount", payment.getAmount().toString());
            put("PaymentId", payment.getId().toString());
            put("PaymentDate", date.toString());
        }};

        if (saveFormat == SaveFormat.PDF || saveFormat == SaveFormat.DOCX) {
            fillValuesInDocumentTemplate(docTemplate, fieldValues);
            docTemplate.save(stream, saveFormat);
        } else if (saveFormat == XLS) {
            saveWordDocumentToExcel(stream, fieldValues);
        }
    }

    private void generateCsvFile(OutputStream stream, String[] headers, List<String[]> data) throws IOException {
        try (
                Writer writer = new BufferedWriter(new OutputStreamWriter(stream, "utf-8"));
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ) {
            if (headers != null && headers.length > 0) {
                csvWriter.writeNext(headers);
            }
            csvWriter.writeAll(data);
            csvWriter.flush();
        }
    }

    private void fillValuesInDocumentTemplate(Document doc, Map<String, String> values) throws Exception {
        BookmarkCollection bookmarks = doc.getRange().getBookmarks();
        for (Map.Entry<String, String> bookmarkValue: values.entrySet()) {
            Bookmark targetBookmark = bookmarks.get(bookmarkValue.getKey());
            if (targetBookmark != null) {
                targetBookmark.setText(bookmarkValue.getValue() != null
                        ? bookmarkValue.getValue()
                        : ""
                );
            }
        }
    }

    private void createDocumentWithSingleTable(OutputStream stream, String[] headers, List<String[]> rowItems, int saveFormat) throws Exception {
        switch (saveFormat) {
            case SaveFormat.DOCX:
            case SaveFormat.PDF: {
                Document doc = new Document();

                DocumentBuilder docBuilder = new DocumentBuilder(doc);
                createTableInWordDocument(docBuilder, headers, rowItems);

                PdfSaveOptions saveOptions = new PdfSaveOptions();

                PdfEncryptionDetails encryptionDetails = new PdfEncryptionDetails("1234", "12345", PdfEncryptionAlgorithm.RC_4_128);
                encryptionDetails.setPermissions(PdfPermissions.DISALLOW_ALL);
                encryptionDetails.setPermissions(PdfPermissions.PRINTING);
                saveOptions.setEncryptionDetails(encryptionDetails);
                saveOptions.setSaveFormat(saveFormat);
                doc.save(stream, saveOptions);
                break;
            }
            default: {
                createExcelDocument(stream, headers, rowItems);
            }
        }
    }

    private void createTableInWordDocument(DocumentBuilder builder, String[] headers, List<String[]> rowItems) {
        builder.startTable();

        if (headers != null && headers.length != 0) {
            createWordTableRow(builder, headers, true);
        }

        for (String[] rowValues: rowItems) {
            createWordTableRow(builder, rowValues, false);
        }

        builder.endTable();
    }

    private void createWordTableRow(DocumentBuilder builder, String[] rowValues, boolean isHeader) {
        for (String cellValue : rowValues) {
            builder.insertCell();
            builder.getFont().setSize(12);
            if (isHeader) {
                builder.getFont().setBold(true);
            } else {
                builder.getFont().setBold(false);
            }
            builder.write(cellValue);
        }
        builder.endRow();
    }


    private void saveWordDocumentToExcel(OutputStream stream, Map<String, String> fieldValues) throws IOException {
        Set<String> tableKeys = fieldValues.keySet();
        Collection<String> tableValues = fieldValues.values();

        String[] allHeaders = (String[])ArrayUtils.addAll(tableKeys.toArray(new String[tableKeys.size()]));
        String[] allValues = (String[])ArrayUtils.addAll(tableValues.toArray(new String[tableValues.size()]));

        createExcelDocument(stream, allHeaders, new ArrayList<String[]>() {{
            add(allValues);
        }});
    }

    private void createExcelDocument(OutputStream stream, String[] headers, List<String[]> tableValues) throws IOException {
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet();

        org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(0);
        for (int i = 0; i < headers.length; i++) {
            org.apache.poi.ss.usermodel.Cell headerCell = headerRow.createCell(i);
            headerCell.setCellValue(headers[i]);
        }
        for (int i = 0; i < tableValues.size(); i++)
        {
            org.apache.poi.ss.usermodel.Row tableRow = sheet.createRow(i + 1);
            for (int j = 0; j < tableValues.get(i).length; j++) {
                org.apache.poi.ss.usermodel.Cell tableCell = tableRow.createCell(j);
                tableCell.setCellValue(tableValues.get(i)[j]);
                sheet.autoSizeColumn(j);
            }
        }

        sheet.setHorizontallyCenter(true);

        book.write(stream);
        book.close();
    }
}
