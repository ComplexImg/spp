package by.bsuir.service;

import by.bsuir.model.Operator;
import by.bsuir.model.PaymentEntity;
import by.bsuir.model.TariffEntity;
import by.bsuir.model.User;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

public interface ReportGenerationService {
    void generateUserPaymentsReport(OutputStream stream, List<PaymentEntity> payments, int saveFormat, Date date) throws Exception;

    void generateUserServicesReport(OutputStream stream, User user, int saveFormat) throws Exception;

    void generateUserContract(OutputStream stream, User user, TariffEntity tariff, Date date, int saveFormat) throws Exception;

    void generatePaymentQuote(OutputStream stream, User user, PaymentEntity payment, Operator operator, Date date, int saveFormat) throws Exception;

    void generateListOfAllOperatorServices(OutputStream stream, Operator operator, int saveFormat) throws Exception;
}
