package by.bsuir.service;


import by.bsuir.model.Operator;
import by.bsuir.model.TariffEntity;

import java.util.List;

public interface TariffService {

    public TariffEntity save(TariffEntity tariffEntity);

    public TariffEntity getById(Integer id);

    public boolean delete(Integer id);

    public List<TariffEntity> getList();



    public List<TariffEntity> getOperatorTariffs(Operator operator);

//
//    public List<TariffEntity> getList(Operator operator);
//
//    public TariffEntity get(TariffEntity tariffEntity);
//
//    public TariffEntity updateUserOperatorTariff(User user, Operator operator);
}
