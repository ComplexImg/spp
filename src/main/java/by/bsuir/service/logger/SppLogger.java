package by.bsuir.service.logger;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


@Service("sppLogger")
public class SppLogger {

    private static Logger logger = null;

    private String logFile = "log.log";

    public void saveStr(String str) {

        if (logger == null) {
            logger = Logger.getLogger("MyLog");
            FileHandler fh;

            try {

                // This block configure the logger with handler and formatter
                fh = new FileHandler(logFile);
                logger.addHandler(fh);
                SimpleFormatter formatter = new SimpleFormatter();
                fh.setFormatter(formatter);

                // the following statement is used to log any messages
                logger.info("SPP LOGGER START");

            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        logger.info(str);

    }

}
