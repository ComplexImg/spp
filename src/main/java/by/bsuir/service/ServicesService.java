package by.bsuir.service;


import by.bsuir.model.Operator;
import by.bsuir.model.ServiceEntity;

import java.util.List;

public interface ServicesService {

//    public List<ServiceEntity> getUserOperatorServices(User user, Operator operator);
//
//    public void deleteServiceFromUserOperatorsList(User user, Operator operator);
//
//    public List<ServiceEntity> getOperatorServices(Operator operator);
//
//    public List<ServiceEntity> getAllUserServices(User user);

    public ServiceEntity getById(Integer id);

    public ServiceEntity save(ServiceEntity serviceEntity);

    public boolean delete(ServiceEntity serviceEntity);

    public List<ServiceEntity> getOperatorServices(Operator operator);
}
