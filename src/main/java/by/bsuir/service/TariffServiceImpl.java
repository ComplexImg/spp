package by.bsuir.service;

import by.bsuir.model.Operator;
import by.bsuir.model.TariffEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service("tariffService")
@Transactional
public class TariffServiceImpl implements TariffService {

    @PersistenceContext
    EntityManager em;

    public TariffEntity save(TariffEntity te) {
        if (te.getId() == null)
            em.persist(te);
        else
            te = em.merge(te);
        return te;
    }

    public TariffEntity getById(Integer id) {
        TariffEntity te = em.find(TariffEntity.class, id);
        return te;
    }

    public boolean delete(Integer id) {
        if (id != null) {

            int res = em.createNativeQuery("delete from tariff where id =:tid").setParameter("tid", id).executeUpdate();
            return res > 0;

//
//            em.remove(em.find(TariffEntity.class, id));
//            em.flush();
//            return true;
        }
        return false;
    }


    public List<TariffEntity> getOperatorTariffs(Operator operator) {

        List<TariffEntity> tariffs = em.createQuery("select c from TariffEntity c where c.owner.id=:findOperator").setParameter("findOperator", operator.getId()).getResultList();
        return tariffs;
    }

    public List<TariffEntity> getList() {
        List<TariffEntity> tariffs = em.createQuery("select c from TariffEntity c ").getResultList();
        return tariffs;
    }


}
