package by.bsuir.service;

import by.bsuir.model.Operator;
import by.bsuir.model.User;
import by.bsuir.model.UsersEntity;

import java.util.List;

public interface UserService {

    //public UsersEntity getUserByLogin(String login);

//    public TariffEntity getUserOperatorTariff(User user, Operator operator);

    public List<User> getAllUsersByTariff(Integer tariffId);

    public List<User> getAllOperatorUsers(Integer operatorId);

    public List<String> getAllUsersCities();

    public UsersEntity getUserByLogin(String login);

    public UsersEntity getUserById(Integer id);

    public User getUserrrById(Integer id);

    public Operator getOperatorByLogin(String login);

    public User gettUserrrByLogin(String login);

    public UsersEntity saveUser(UsersEntity user);

    public List<Operator> getOperatorsList();

    public List<User> getUsersList();

    public Operator getOperatorById(Integer id);

    public Operator getFullOperatorById(Integer id);

    public boolean delete(UsersEntity usersEntity);

    public boolean updateUserOperatorTarif(Integer userId, Integer operatorId, Integer tarifId);

    public Integer userOperatorTarif(Integer userId, Integer operatorId);
//
//    public UsersEntity saveUser(UsersEntity user);
//
//    public List<Operator> getAllOperators();
//
//    public List<User> getAllUsers();
//
//    public List<User> getOperatorUsersWithTariff(Operator operator);
//
//    public List<Operator> getUserOperators(User user);
//
//    public void deleteOperatorFromUserOperatorsList(User user, Operator operator);
}
