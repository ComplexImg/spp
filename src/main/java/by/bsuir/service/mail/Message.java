package by.bsuir.service.mail;


import by.bsuir.model.User;

import java.io.Serializable;


public class Message implements Serializable {
    private String email;
    private String subject;
    private String body;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Message() {

    }
    public Message(User u) {
        this.email = u != null?u.getEmail():"";
    }

    public Message(String email) {
        this.email = email;
    }
}
