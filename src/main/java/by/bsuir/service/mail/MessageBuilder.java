package by.bsuir.service.mail;

import by.bsuir.model.User;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service("messageBuilder")
public class MessageBuilder {


    private MimeMessage message;
    private MimeMessageHelper helper;

    public MessageBuilder init(User user, JavaMailSender mailSender) {
        try {
            this.message = mailSender.createMimeMessage();
            this.helper = new MimeMessageHelper(message, "utf-8");
            if(user != null)
                this.helper.setTo(user.getEmail());
            this.message.setFrom("SppProject@yandex.by");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return this;
    }

    public MessageBuilder subject(String subj) throws Exception{
        helper.setSubject(subj);
        return this;
    }
    public MessageBuilder to(String email) throws Exception{
        helper.setTo(email);
        return this;
    }
    public MessageBuilder body(String body, boolean html) throws Exception{
        helper.setText(body, html);
        return this;
    }

    public MimeMessage construct(){
        return message;
    }
}
