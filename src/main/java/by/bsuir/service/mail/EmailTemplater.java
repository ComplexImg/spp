package by.bsuir.service.mail;


import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("emailTemplater")
public class EmailTemplater {

    private String nameReg = "${name}";
    private String themeReg = "${theme}";
    private String textReg = "${text}";

    private List<String> templates;


    public List<String> getTemplates() {
        return templates;
    }

    public EmailTemplater() {
        templates = new ArrayList<>();

        templates.add("<html>\n" +
                "<head>\n" +
                "<meta charset='UTF-8'>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h3 style='color: yellow; background: blue'>${theme}</h3>\n" +
                "<h3>${name}</h3>\n" +
                "<div>${text}</div>\n" +
                "</body>\n" +
                "</html>");


        templates.add("<html>\n" +
                "<head>\n" +
                "<meta charset='UTF-8'>\n" +
                "</head>\n" +
                "<body style='font-style: italic'>\n" +
                "<h3 style='color: black; background: red'>${theme}</h3>\n" +
                "<h3>${name}</h3>\n" +
                "<div>${text}</div>\n" +
                "</body>\n" +
                "</html>");


        templates.add("<html>\n" +
                "<head>\n" +
                "<meta charset='UTF-8'>\n" +
                "</head>\n" +
                "<body style='text-align: center'>\n" +
                "<h3>${theme}</h3>\n" +
                "<h3>${name}</h3>\n" +
                "<div style='border: 1px solid green'>${text}</div>\n" +
                "</body>\n" +
                "</html>");
    }

    public String getEmail(Integer templateId, String text, String user, String theme) {

        String template = templates.get(templateId);

        template = template.replace(nameReg, user);
        template = template.replace(textReg, text);
        template = template.replace(themeReg, theme);

        return template;

    }
}
