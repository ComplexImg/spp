package by.bsuir.service.mail;

import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;


public class EmailSender extends Thread {

    public EmailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    private JavaMailSender mailSender;

    private MimeMessage message;

    public EmailSender set(MimeMessage message){
        this.message = message;
        return this;
    }

    @Override
    public void run() {
        try {
            mailSender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
}
