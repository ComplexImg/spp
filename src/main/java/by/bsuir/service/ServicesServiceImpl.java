package by.bsuir.service;

import by.bsuir.model.Operator;
import by.bsuir.model.ServiceEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


@org.springframework.transaction.annotation.Transactional
@Service("servicesService")
public class ServicesServiceImpl implements ServicesService {

    @PersistenceContext
    EntityManager em;


    public List<ServiceEntity> getOperatorServices(Operator operator){

        List<ServiceEntity> services = em.createQuery("select c from ServiceEntity c where c.owner.id=:findOperator").setParameter("findOperator", operator.getId()).getResultList();
        return services;
    }

    public ServiceEntity save(ServiceEntity serviceEntity) {

        if (serviceEntity.getId() == null) {
            em.persist(serviceEntity);
        } else {
            serviceEntity = em.merge(serviceEntity);
        }
        return serviceEntity;

    }

    public ServiceEntity getById(Integer id){
        ServiceEntity te = em.find(ServiceEntity.class, id);
        return te;
    }


    public boolean delete(ServiceEntity serviceEntity) {
        if (serviceEntity.getId() != null) {

            int res = em.createNativeQuery("delete from service where id =:tid").setParameter("tid", serviceEntity.getId()).executeUpdate();
            return res > 0;

//            em.remove(em.find(ServiceEntity.class, serviceEntity.getId()));
//            return true;
        }
        return false;
    }


}
