package by.bsuir.service;

import by.bsuir.model.Operator;
import by.bsuir.model.User;
import by.bsuir.model.UsersEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {

    @PersistenceContext
    private EntityManager em;

    public List<User> getAllOperatorUsers(Integer operatorId){

//        List<User> list = (List<User>)em.createNativeQuery("SELECT * FROM users JOIN user_operators ON users.id = user_operators.user_id  where users.id=:id").setParameter("id", operatorId).getResultList();
        return em.createQuery("select u from User u join fetch u.operators o where o.id =:operatorId").setParameter("operatorId", operatorId).getResultList();
    }
    public List<User> getAllUsersByTariff(Integer tariffId){

//        List<User> list = (List<User>)em.createNativeQuery("SELECT * FROM users JOIN user_operators ON users.id = user_operators.user_id  where users.id=:id").setParameter("id", operatorId).getResultList();
        return em.createQuery("select u from User u join fetch u.operators o join fetch o.tarifs t where t.id =:tariffId").setParameter("tariffId", tariffId).getResultList();
    }

    public List<String> getAllUsersCities(){
        List<String> res = em.createNativeQuery("select town from user_info").getResultList();
        return res;
    }

    public UsersEntity getUserById(Integer id){
        UsersEntity te = em.find(UsersEntity.class, id);
        return te;
    }

    public Operator getFullOperatorById(Integer id){
        return (Operator)em.createQuery("select o from Operator o join fetch o.services  where o.id=:id").setParameter("id", id).getSingleResult();
    }

    public boolean delete(UsersEntity usersEntity) {
        if (usersEntity.getId() != null) {
            em.remove(em.find(UsersEntity.class, usersEntity.getId()));
            return true;
        }
        return false;
    }


    public List<Operator> getOperatorsList() {
        List<Operator> operators = em.createQuery("select c from Operator c ").getResultList();
        return operators;
    }

    public Operator getOperatorById(Integer id){
        Operator te = em.find(Operator.class, id);
        return te;
    }

    public User getUserrrById(Integer id){
        User te = em.find(User.class, id);
        return te;
    }

    public List<User> getUsersList() {
        List<User> users = em.createQuery("select c from User c ").getResultList();
        return users;
    }


    public UsersEntity getUserByLogin(String login) {
        List<UsersEntity> users = em.createQuery("select c from UsersEntity c where c.username=:findLogin").setParameter("findLogin", login).getResultList();
        if (users.size() != 1) {
            return null;
        } else {
            return users.get(0);
        }
    }

    public Operator getOperatorByLogin(String login){
        List<Operator> users = em.createQuery("select c from Operator c where c.username=:findLogin").setParameter("findLogin", login).getResultList();
        if (users.size() != 1) {
            return null;
        } else {
            return users.get(0);
        }
    }

    public User gettUserrrByLogin(String login){
        List<User> users = em.createQuery("select c from User c where c.username=:findLogin").setParameter("findLogin", login).getResultList();
        if (users.size() != 1) {
            return null;
        } else {
            return users.get(0);
        }
    }

    public UsersEntity saveUser(UsersEntity user) {
        if (user.isValid()) {
            if (user.getId() == null) {
                em.persist(user);
            } else {
                user = em.merge(user);
            }
            return user;
        }
        return null;
    }

    public boolean updateUserOperatorTarif(Integer userId, Integer operatorId, Integer tarifId){
        int res = em.createNativeQuery("update user_operators set tariff_id =:tid where user_id =:uid and operator_id=:oid").
                setParameter("tid", tarifId).
                setParameter("uid", userId).
                setParameter("oid", operatorId).executeUpdate();
        return res > 0;
    }

    public Integer userOperatorTarif(Integer userId, Integer operatorId){
        Object res = em.createNativeQuery("select tariff_id from user_operators where user_id =:uid and operator_id=:oid").
                setParameter("uid", userId).
                setParameter("oid", operatorId).getSingleResult();
        return (Integer)res;
    }
}
