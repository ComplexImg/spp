<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <title>${title}</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/swiper.min.css"/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js"/>"></script>
    <script type="application/javascript" src="<c:url value="/resources/js/swiper.min.js"/>"></script>
</head>

<body>
<div id="header-wrapper">
    <div id="header" class="container">
        <div id="logo">
            <h1><a href="#">Sl-Le-Va</a></h1>
            <p>Slava Leha Vanya</p>
        </div>
        <div id="menu">
            <ul>
                <li><a href="<c:url value="/index"/>"><spring:message code="Main"/></a></li>
                <li><a href="<c:url value="/register"/>"><spring:message code="Register"/></a></li>
                <li><a href="<c:url value="/login"/>"><spring:message code="Login"/></a></li>
                <li><a href="<c:url value="/about"/>"><spring:message code="about"/></a></li>
                <li>
                    <%
                        String locale = "";
                        if (request.getCookies() != null) {
                            for (Cookie c : request.getCookies()) {
                                if (c.getName().equals("locale")) {
                                    locale = c.getValue();
                                    break;
                                }
                            }
                        }


                    %>

                    <form style="padding: 9px 10px;">
                        <select name="lang" onchange="this.form.submit()">
                            <c:forEach items="ru_RU,en_EN" var="i">
                                <%
                                    String i = (String) pageContext.getAttribute("i");
                                %>

                                <option value="${i}" <%if(locale.equals(i)){%>selected<%}%>>${i}</option>
                            </c:forEach>
                        </select>
                    </form>


                </li>
            </ul>
        </div>
    </div>
    <div id="banner" class="container">
        <div class="image-style">


            <!-- Swiper -->
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="<c:url value="/resources/images/1.jpeg"/>" width="970" height="320" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="<c:url value="/resources/images/2.jpg"/>" width="970" height="320" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="<c:url value="/resources/images/3.jpg"/>" width="970" height="320" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="<c:url value="/resources/images/4.jpg"/>" width="970" height="320" alt=""/>
                    </div>
                    <div class="swiper-slide">
                        <img src="<c:url value="/resources/images/5.jpg"/>" width="970" height="320" alt=""/>
                    </div>

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>

            <!-- Initialize Swiper -->
            <script>
                var swiper = new Swiper('.swiper-container', {
                    loop: true,
                    lazy: true,
                    autoplay: true,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                });
            </script>

        </div>
    </div>
</div>



<div id="wrapper">
    <div id="splash" class="container"><span><spring:message code="splash_span"/> </span><spring:message code="splash"/>
    </div>
    <div id="page" class="container">
        <div id="content">

            <jsp:include page="${view}"/>

        </div>
        <!-- end #content -->
        <div id="sidebar">
            <jsp:include page="${menu}"/>

            <div>
                <h2>Nulsdfdsfd</h2>
                <ul class="list-style1">
                    <li class="first"><a href="#">Aliquam libero</a></li>
                    <li><a href="#">Consectetuer adipiscing elit</a></li>
                    <li><a href="#">Metus aliquam pellentesque</a></li>
                    <li><a href="#">Suspendisse iaculis mauris</a></li>
                    <li><a href="#">Urnanet non molestie semper</a></li>
                    <li><a href="#">Proin gravida orci porttitor</a></li>
                </ul>
            </div>
        </div>
        <!-- end #sidebar -->
        <div style="clear: both;">&nbsp;</div>
    </div>
    <!-- end #page -->
</div>
<div id="footer-content" class="container">
    <div id="footer-bg">
        <div id="column1">
            <h2>Welcome to my website</h2>
            <p>In posuere eleifend odio quisque semper augue mattis wisi maecenas ligula. Donec leo, vivamus fermentum
                nibh in augue praesent a lacus at urna congue rutrum. Quisque dictum integer nisl risus, sagittis
                convallis, rutrum id, congue, and nibh. Pellentesque tristique ante ut risus. Quisque dictum. Integer
                nisl risus, sagittis convallis, rutrum id, elementum congue, nibh. Suspendisse dictum porta lectus.<br/>
            </p>
        </div>
        <div id="column2">
            <h2>Etiam rhoncus volutpat</h2>
            <ul class="list-style2">
                <li class="first"><a href="#">Pellentesque quis elit non lectus gravida blandit.</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
                <li><a href="#">Phasellus nec erat sit amet nibh pellentesque congue.</a></li>
                <li><a href="#">Cras vitae metus aliquam risus pellentesque pharetra.</a></li>
                <li><a href="#">Maecenas vitae orci vitae tellus feugiat eleifend.</a></li>
            </ul>
        </div>
        <div id="column3">
            <h2>Recommended Links</h2>
            <ul class="list-style2">
                <li class="first"><a href="#">Pellentesque quis elit non lectus gravida blandit.</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
                <li><a href="#">Phasellus nec erat sit amet nibh pellentesque congue.</a></li>
                <li><a href="#">Cras vitae metus aliquam risus pellentesque pharetra.</a></li>
                <li><a href="#">Maecenas vitae orci vitae tellus feugiat eleifend.</a></li>
            </ul>
        </div>
    </div>
</div>
<div id="footer">
    <p>© 2012 Untitled Inc. All rights reserved. Lorem ipsum dolor sit amet nullam blandit consequat phasellus etiam
        lorem. Design by <a href="http://templated.co" rel="nofollow">TEMPLATED</a>. Photos by <a
                href="http://fotogrph.com/">Fotogrph</a>.</p>
</div>
<!-- end #footer -->
</body>
</html>
