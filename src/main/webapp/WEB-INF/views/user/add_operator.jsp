<%@ page import="by.bsuir.model.User" %>
<%@ page import="by.bsuir.model.Operator" %>
<%@ page import="by.bsuir.model.TariffEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="operator" value="${operator}"/>
<c:set var="user" value="${user}"/>
<%
    User user = (User) pageContext.getAttribute("user");
    Operator operator = (Operator) pageContext.getAttribute("operator");
%>
<h2 class="title"><spring:message code="add_operator"/></h2>

<form class="form">

    <input name="field_1" type="hidden" value="${user.getId()}">
    <input name="field_2" type="hidden" value="${operator.getId()}">

    <select name="field_3" required>
        <c:forEach items="${operator.getTarifs()}" var="tariff">
            <%
                TariffEntity tariff = (TariffEntity) pageContext.getAttribute("tariff");
            %>
            <option value="${tariff.getId()}">${tariff.getTitle()}</option>

        </c:forEach>
    </select>

    <button id="btn"><spring:message code="save"/></button>
</form>

<h2 class="title"><spring:message code="service"/></h2>

<table style="width: 100%" id="myTable">
    <tr>
        <th><spring:message code="title"/></th>
        <th><spring:message code="cost"/></th>
        <th><spring:message code="ussd"/></th>
        <th><spring:message code="description"/></th>
    </tr>
    <c:forEach items="${operator.getServices()}" var="service">
        <tr>
            <td>${service.getTitle()}</td>
            <td>${service.getCostPerMonth()}</td>
            <td>${service.getUssdCode()}</td>
            <td>${service.getDescription()}</td>
        </tr>
    </c:forEach>

</table>

<script type="application/javascript" src="<c:url value="/resources/js/slider.js"/>"></script>

<script>

    $(".form").submit(function () {

        var form = $(this);

        $.post("/mvc/user/add_operator", form.serialize(), function (resp) {

            alert("<spring:message code="all_right"/>");

            console.log(resp);

        });

        return false;

    })

</script>
