<%@ page import="by.bsuir.model.User" %>
<%@ page import="by.bsuir.model.Operator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="operatorsList" value="${operatorsList}"/>
<c:set var="user" value="${user}"/>
<%
    User user = (User) pageContext.getAttribute("user");
%>

<h2 class="title"><spring:message code="operators_list"/></h2>


<table style="width: 100%" id="myTable">
    <tr>
        <th><spring:message code="title"/></th>
        <th><spring:message code="description"/></th>
        <th><spring:message code="email"/></th>
        <th style="width: 50px"></th>
    </tr>

    <c:forEach items="${operatorsList}" var="operator">
        <%
            Operator operator = (Operator) pageContext.getAttribute("operator");

        %>

        <tr <%if (user.isOperatorInUser(operator)) {%>style="background: #e8efe9"<%}%>>
            <td>${operator.getTitle()}</td>
            <td>${operator.getDescription()}</td>
            <td><a href="mailto:${operator.getEmail()}">${operator.getEmail()}</a></td>
            <td>
                <%
                    if (user.isOperatorInUser(operator)) {
                %>
                <a href="<c:url value="/user/edit_operator/${operator.getId()}"/>"><spring:message code="edit"/></a>
                <%
                } else {
                %>
                <a href="<c:url value="/user/add_operator/${operator.getId()}"/>"><spring:message code="add"/></a>
                <%
                    }
                %>
            </td>

        </tr>
    </c:forEach>
</table>

<script type="application/javascript" src="<c:url value="/resources/js/slider.js"/>"></script>
