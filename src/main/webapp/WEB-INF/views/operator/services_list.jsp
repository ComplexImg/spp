<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h2 class="title"><spring:message code="services_list"/></h2>


<form class="form">

    <table style="width: 100%" id="myTable">
        <tr>
            <th><spring:message code="title"/></th>
            <th><spring:message code="cost"/></th>
            <th><spring:message code="description"/></th>
            <th><spring:message code="ussd"/></th>
            <th><spring:message code="edit"/></th>
            <th style="width: 50px"><spring:message code="delete"/></th>
        </tr>

        <c:forEach items="${servicesList}" var="service">
            <tr>
                <td>${service.getTitle()}</td>
                <td>${service.getCostPerMonth()}</td>
                <td>${service.getUssdCode()}</td>
                <td>${service.getDescription()}</td>
                <td>
                    <a href="<c:url value="/operator/edit_service/${service.getId()}"/>"><spring:message
                            code="edit"/></a>
                </td>
                <td>
                    <input type="checkbox" name="int_list" value="${service.getId()}">
                </td>
            </tr>
        </c:forEach>
    </table>

    <script type="application/javascript" src="<c:url value="/resources/js/slider.js"/>"></script>

    <button class="delete"><spring:message code="delete"/></button>


</form>



<script>


    $(".form").submit(function () {

        var form = $(this);

        $.post("/mvc/operator/delete_services", form.serialize(), function (resp) {
            location.reload();
        });

        return false;

    })


    //    $("button[class=delete]").click(function () {
    //
    //        var deleted_id = $(this).data("deleted_id");
    //
    //        $(this).parents("tr").hide("slow");
    //
    //        $.post("/mvc/operator/delete_service/" + deleted_id, {}, function (answer) {
    //
    //            console.log(answer);
    //
    //        });
    //
    //    });

</script>

