<%@ page import="by.bsuir.model.TariffEntity" %>
<%@ page import="by.bsuir.model.ServiceEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="tariffEntity" value="${tariffEntity}"/>
<h2 class="title"><spring:message code="tariff"/></h2>

<form class="form">
    <input type="hidden" name="id" value="${tariffEntity.getId()}"/>

    <table style="width:100%;">
        <tr>
            <td>
                <spring:message code="title"/>
            </td>
            <td>
                <input type="text" pattern="[\S]{3,30}" required name="title" placeholder="Title" value="${tariffEntity.getTitle()}"/>
            </td>
        </tr>

        <tr>
            <td>
                <spring:message code="description"/>
            </td>
            <td>
                <textarea maxlength="250" name="description">${tariffEntity.getDescription()}</textarea>
            </td>
        </tr>

        <tr>
            <td>
                <spring:message code="cost"/>
            </td>
            <td>
                <input type="text" pattern="[0-9]{1,10}" required name="costPerMonth" placeholder="costPerMonth"
                       value="${tariffEntity.getCostPerMonth()}"/>
            </td>
        </tr>

    </table>

    <button name="regSubmit"><spring:message code="save"/></button>
</form>

<c:if test="${not empty tariffEntity}">

    <h2 class="title"><spring:message code="service"/></h2>

    <table style="width: 100%;">
        <tr>
            <th><spring:message code="title"/></th>
            <th><spring:message code="cost"/></th>
            <th><spring:message code="ussd"/></th>
            <th><spring:message code="description"/></th>
            <th style="width: 20px"><spring:message code="in"/> ?</th>
        </tr>
        <c:forEach items="${servicesList}" var="service">
            <tr>
                <td>${service.getTitle()}</td>
                <td>${service.getCostPerMonth()}</td>
                <td>${service.getUssdCode()}</td>
                <td>${service.getDescription()}</td>
                <td>
                    <%
                        ServiceEntity service = (ServiceEntity) pageContext.getAttribute("service");
                        TariffEntity te = (TariffEntity) pageContext.getAttribute("tariffEntity");
                        te.isServiceInTariffServices(service);
                    %>
                    <input data-service_id="${service.getId()}" class="tariff-checkbox" type="checkbox" value=""
                           <%if(te.isServiceInTariffServices(service)){%>checked="checked"<%}%>>
                </td>
            </tr>
        </c:forEach>

    </table>

</c:if>


<script>


    $("input[type=checkbox]").click(function () {


        var service_id = $(this).data('service_id');
        var checked = $(this).is(":checked");
        var tariff_id = $(".form input[name=id]").val();

        var data = {
            field_1: service_id,
            field_2: tariff_id,
            string_1: checked,
        };

        $.post("/mvc/operator/save_or_delete_service_in_tariff", data, function (resp) {

            console.log(resp);

        });

    });


    $(".form").submit(function () {

        var form = $(this);

        $.post("/mvc/operator/save_tariff", form.serialize(), function (answer) {

            alert("<spring:message code="all_right"/>");

            console.log(answer);

        });

        return false;

    });

</script>