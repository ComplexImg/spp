<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div>
    <h2><spring:message code="Hi"/>, ${userName}</h2>
    <ul class="list-style1">
        <li  class="first"><a href="<c:url value="/operator"/>"><spring:message code="main"/></a></li>
        <li><a href="<c:url value="/operator/services_list"/>"><spring:message code="services_list"/></a></li>
        <li><a href="<c:url value="/operator/add_service"/>"><spring:message code="add_service"/></a></li>
        <li><a href="<c:url value="/operator/tariffs_list"/>"><spring:message code="tariffs_list"/></a></li>
        <li><a href="<c:url value="/operator/add_tariff"/>"><spring:message code="add_tariff"/></a></li>
        <li><a href="<c:url value="/operator/edit_info"/>"><spring:message code="edit_info"/></a></li>
        <li><a href="<c:url value="/operator/users_list"/>"><spring:message code="users_list"/></a></li>
        <li><a href="<c:url value="/logout"/>"><spring:message code="logout"/></a></li>
    </ul>
</div>