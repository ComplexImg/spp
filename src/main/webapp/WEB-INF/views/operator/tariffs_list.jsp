<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h2 class="title"><spring:message code="tariffs_list"/></h2>
<table style="width: 100%" id="myTable">
    <tr>
        <th><spring:message code="title"/></th>
        <th><spring:message code="cost"/></th>
        <th><spring:message code="description"/></th>
        <th><spring:message code="edit"/></th>
        <th style="width: 50px"><spring:message code="delete"/></th>
    </tr>

    <c:forEach items="${tariffsList}" var="tariff">
        <tr>
            <td>${tariff.getTitle()}</td>
            <td>${tariff.getCostPerMonth()}</td>
            <td>${tariff.getDescription()}</td>
            <td>
                <a href="<c:url value="/operator/edit_tariff/${tariff.getId()}"/>"><spring:message code="edit"/></a>
            </td>
            <td>
                <button class="delete" data-deleted_id="${tariff.getId()}"><spring:message code="delete"/></button>
            </td>
        </tr>
    </c:forEach>
</table>

<script type="application/javascript" src="<c:url value="/resources/js/slider.js"/>"></script>


<script>


    $("button[class=delete]").click(function () {

        var deleted_id = $(this).data("deleted_id");

        $(this).parents("tr").hide("slow");

        $.post("/mvc/operator/delete_tariff/" + deleted_id, {} , function (answer) {

            console.log(answer);

        });

    });

</script>

