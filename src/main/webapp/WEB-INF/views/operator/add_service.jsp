<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h2 class="title"><spring:message code="service"/></h2>

<form class="form">
    <input type="hidden" name="id" placeholder="Title" value="${serviceEntity.getId()}"/>
    <table style="width: 100%">
        <tr>
            <td>
                <spring:message code="title"/>
            </td>
            <td>
                <input type="text" pattern="[\S]{3,30}" required name="title" placeholder="Title" value="${serviceEntity.getTitle()}"/>
            </td>
        </tr>
        <tr>
            <td>
                <spring:message code="description"/>
            </td>
            <td>
                <textarea  maxlength="250" name="description">${serviceEntity.getDescription()}</textarea>
            </td>
        </tr>

        <tr>
            <td>
                <spring:message code="ussd"/>
            </td>
            <td>
                <input type="text" name="ussdCode" pattern="[0-9]{3,10}" required placeholder="ussdCode" value="${serviceEntity.getUssdCode()}"/>
            </td>
        </tr>

        <tr>
            <td>
                <spring:message code="cost"/>
            </td>
            <td>
                <input type="text" name="costPerMonth" pattern="[0-9]{1,10}" required placeholder="costPerMonth"
                       value="${serviceEntity.getCostPerMonth()}"/>
            </td>
        </tr>
    </table>

    <button name="regSubmit"><spring:message code="save"/></button>

</form>


<script>
    $(".form").submit(function () {

        var form = $(this);

        $.post("/mvc/operator/save_service", form.serialize(), function (answer) {

            alert("<spring:message code="all_right"/>");

            console.log(answer);

        });

        return false;

    });

</script>
